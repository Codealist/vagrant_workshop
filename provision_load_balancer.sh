#!/bin/bash

sudo debconf-set-selections <<< 'mysql-server mysql-server/root_password password admin'
sudo debconf-set-selections <<< 'mysql-server mysql-server/root_password_again password admin'
apt-get install -y mysql-server nginx

cat << EOF > /etc/nginx/sites-available/load_balancer
upstream nodes {
    # ip_hash;
    server 192.168.50.10;
    server 192.168.50.11;
}

server {
    listen 80;

    location / {
        proxy_set_header X-Forwarded-For \$remote_addr;
        proxy_set_header X-Forwarded-Host \$host;
        proxy_pass http://nodes;
    }
}
EOF

rm /etc/nginx/sites-enabled/*
ln -s /etc/nginx/sites-available/load_balancer /etc/nginx/sites-enabled/load_balancer
service nginx restart

echo "CREATE DATABASE IF NOT EXISTS symfony" | mysql -uroot -padmin
cat << EOF | mysql -uroot -padmin
CREATE TABLE IF NOT EXISTS symfony.session (
    sess_id VARBINARY(128) NOT NULL PRIMARY KEY,
    sess_data BLOB NOT NULL,
    sess_time INTEGER UNSIGNED NOT NULL,
    sess_lifetime MEDIUMINT NOT NULL
) COLLATE utf8_bin, ENGINE = InnoDB;
EOF
echo "GRANT ALL ON symfony.* TO 'dbuser'@'%' IDENTIFIED BY 'dbuser'" | mysql -uroot -padmin

cat << EOF > /etc/mysql/conf.d/application.cnf
[mysqld]
bind-address = 0.0.0.0
EOF

service mysql restart
